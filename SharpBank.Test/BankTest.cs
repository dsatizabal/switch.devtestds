﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void TestDuplicateCustomer()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account(AccountType.Checking));
            bank.AddCustomer(john);

            Customer john2 = new Customer("JOHN");
            john2.OpenAccount(new Account(AccountType.Checking));

            try
            {
                bank.AddCustomer(john2);
            }
            catch (ArgumentException aex)
            {
                Assert.AreEqual(aex.Message, "Customer already exists: JOHN");
                return;
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected exception wasn't thrown: ArgumentException expected, " + ex.GetType() + " raised");
                return;
            }

            Assert.Fail("TestDuplicateCustomer Fail: An exception was expected");
        }

        [Test]
        public void BadCustomerTest()
        {
            Bank bank = new Bank();
            Customer john = null;

            try
            {
                bank.AddCustomer(john);
            }
            catch (ArgumentException aex)
            {
                Assert.AreEqual(aex.Message, "Null customer data");
                return;
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected exception wasn't thrown: ArgumentException expected, " + ex.GetType() + " raised");
                return;
            }

            Assert.Fail("BadCustomerTest Fail: An exception was expected");
        }

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account(AccountType.Checking));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Checking);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Savings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);

            //Assert.AreEqual(170.0, bank.TotalInterestPaid(), DOUBLE_DELTA); 170 corresponds to old interests range
            Assert.AreEqual(150.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountRecentDebit()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);
            checkingAccount.Withdraw(1000.0);

            //Assert.AreEqual(170.0, bank.TotalInterestPaid(), DOUBLE_DELTA); 170 corresponds to old interests range
            Assert.AreEqual(2, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }


        [Test]
        public void InterestPaidSimpleTest()
        {
            Bank bank = new Bank();

            Customer BILL = new Customer("Bill");
            bank.AddCustomer(BILL);

            BILL.OpenAccount(new Account(AccountType.Checking));
            BILL.getAccount(AccountType.Checking).Deposit(3000);

            Assert.AreEqual("Interests paid consolidated: \n\nTotal interets paid: $3,00\n", bank.InterestPaidReport(false));
        }

        [Test]
        public void InterestPaidDetailesTest()
        {
            Bank bank = new Bank();

            Customer BILL = new Customer("Bill");
            bank.AddCustomer(BILL);
            BILL.OpenAccount(new Account(AccountType.Checking));
            BILL.getAccount(AccountType.Checking).Deposit(3000);

            Customer DONALD = new Customer("Donald");
            bank.AddCustomer(DONALD);
            DONALD.OpenAccount(new Account(AccountType.Checking));
            DONALD.getAccount(AccountType.Checking).Deposit(2000);

            Assert.AreEqual("Interests paid by customer: \n\nBill: $3,00\nDonald: $2,00\n\nTotal interets paid: $5,00\n", bank.InterestPaidReport(true));
        }


    }
}
