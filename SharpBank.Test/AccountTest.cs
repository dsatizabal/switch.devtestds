﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Test
{
    class AccountTest
    {
        [Test]
        public void NegativeDepositTest()
        {
            Account A = new Account(AccountType.Checking);

            try
            {
                A.Deposit(-100);
            }
            catch(ArgumentException aex)
            {
                Assert.AreEqual("amount must be greater than zero", aex.Message);
                return;
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected exception wasn't thrown: ArgumentException expected, " + ex.GetType() + " raised");
                return;
            }

            Assert.Fail("NegativeDepositTest Fail: An exception was expected");
        }


        [Test]
        public void BalanceExceededTest()
        {
            Account A = new Account(AccountType.Checking);
            A.Deposit(1000);
            A.Withdraw(500);

            try
            {
                A.Withdraw(600);
            }
            catch (ArgumentException aex)
            {
                Assert.AreEqual("amount exceedes account's balance", aex.Message);
                return;
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected exception wasn't thrown: ArgumentException expected, " + ex.GetType() + " raised");
                return;
            }

            Assert.Fail("NegativeDepositTest Fail: An exception was expected");
        }
    }
}
