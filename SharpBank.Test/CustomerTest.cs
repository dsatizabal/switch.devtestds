﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account(AccountType.Checking);
            Account savingsAccount = new Account(AccountType.Savings);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                            "Checking Account\n\n" +
                            "  Operation               Date      Amount \n" +
                            "  deposit             " + DateTime.Now.ToString("yyyy-MM-dd") + "   $100,00\n\n" +
                            "Account balance $100,00\n" +
                            "Savings Account\n\n" +
                            "  Operation               Date      Amount \n" +
                            "  deposit             " + DateTime.Now.ToString("yyyy-MM-dd") + "   $4.000,00\n" +
                            "  withdrawal          " + DateTime.Now.ToString("yyyy-MM-dd") + "   $200,00\n\n" +
                            "Account balance $3.800,00\n\n" +
                            "Total In All Accounts $3.900,00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(AccountType.Savings));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(AccountType.Savings));
            oscar.OpenAccount(new Account(AccountType.Checking));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(AccountType.Savings));
            oscar.OpenAccount(new Account(AccountType.Checking));
            oscar.OpenAccount(new Account(AccountType.MaxiSavings));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestDuplicateAccountType()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(AccountType.Savings));
            Assert.AreEqual(null, oscar.OpenAccount(new Account(AccountType.Savings)));
        }
    }
}
