﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Test
{
    class TransferTest
    {
        [Test]
        public void SameAccountTransfer()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Checking);
            bank.AddCustomer(new Customer("Carlos").OpenAccount(checkingAccount));

            Customer Carlos = bank.getCustomer("Carlos");

            Account savingsAccount = new Account(AccountType.Savings);
            Carlos.OpenAccount(savingsAccount);
            savingsAccount.Deposit(1500);

            savingsAccount.transfer(ref checkingAccount, 500);

            Assert.AreEqual(1000, savingsAccount.balance());
        }

        [Test]
        public void InterAccountTransfer()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Checking);
            bank.AddCustomer(new Customer("Carlos").OpenAccount(checkingAccount));

            checkingAccount = new Account(AccountType.Checking);
            bank.AddCustomer(new Customer("Diego").OpenAccount(checkingAccount));

            Customer Carlos = bank.getCustomer("Carlos");
            Customer Diego = bank.getCustomer("Diego");

            Carlos.getAccount(AccountType.Checking).Deposit(2500);
            Diego.getAccount(AccountType.Checking).Deposit(300);

            Account dstAcc = Diego.getAccount(AccountType.Checking);
            Carlos.getAccount(AccountType.Checking).transfer(ref dstAcc, 1500);

            Assert.AreEqual(1800, Diego.getAccount(AccountType.Checking).balance());
        }

    }
}
