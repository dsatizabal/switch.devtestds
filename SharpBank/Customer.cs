﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    //******************************************************************************************************************************************************
    //It's assumed that a customer cannot have two accounts of the same type
    //******************************************************************************************************************************************************

    public class Customer
    {
        public readonly string name;
        private List<Account> accounts;

        #region "Customer Data"

        public Customer(string name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public string GetName()
        {
            return name;
        }

        #endregion


        #region "Accounts Management"

        public Customer OpenAccount(Account account)
        {
            if (account == null)
                return null;

            for (int a = 0; a < this.accounts.Count; a++)
            {
                if (this.accounts[a].accountType == account.accountType)
                    return null;
            }

            accounts.Add(account);
            return this;
        }

        public Account getAccount(AccountType Type)
        {
            if (accounts.Count == 0)
                return null;

            for(int a = 0; a < accounts.Count; a++)
            {
                if (accounts[a].accountType == Type)
                    return accounts[a];
            }

            return null;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();

            return total;
        }

        #endregion


        #region "Statemens"


        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += StatementForAccount(a) + "\n";
                total += a.balance();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.accountType)
            {
                case AccountType.Checking:
                    s += "Checking Account\n\n";
                    break;
                case AccountType.Savings:
                    s += "Savings Account\n\n";
                    break;
                case AccountType.MaxiSavings:
                    s += "Maxi Savings Account\n\n";
                    break;
            }

            s += "  Operation               Date      Amount \n";

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                if (t.type == TransactionType.DirectDeposit) s +=    "  deposit             ";
                if (t.type == TransactionType.DirectWithdrawal) s += "  withdrawal          ";
                if (t.type == TransactionType.IncomingTransfer) s += "  transfer received   ";
                if (t.type == TransactionType.OutgoingTransfer) s += "  transfer sent       ";

                s += t.transactionDate.ToString("yyyy-MM-dd") + "   " + ToDollars(t.amount) + "\n";

                if(t.type == (int)TransactionType.DirectDeposit || t.type == TransactionType.IncomingTransfer)
                {
                    total += t.amount;
                }
                else
                {
                    total -= t.amount;
                }
            }

            s += "\nAccount balance " + ToDollars(total);
            return s;
        }

        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }

        #endregion
    }
}
