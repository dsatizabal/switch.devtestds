﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentException("Null customer data");

            //Not allowing to create same name customer to use customer's name as pivot data
            for (int c = 0; c < customers.Count; c++)
            {
                if (customers[c].name.ToUpper() == customer.name.ToUpper())
                    throw new ArgumentException("Customer already exists: " + customer.name);
            }

            customers.Add(customer);
        }


        public Customer getCustomer(string customerName)
        {
            for (int c = 0; c < customers.Count; c++)
            {
                if (customers[c].name.ToUpper() == customerName.ToUpper())
                    return customers[c];
            }

            return null;
        }

        public String CustomerSummary()
        {
            String summary = "Customer Summary";
            foreach (Customer c in customers)
                summary += "\n - " + c.GetName() + " (" + pluralize(c.GetNumberOfAccounts(), "account") + ")";
            return summary;
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string pluralize(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;

            if (customers.Count == 0)
                return 0;

            foreach (Customer c in customers)
            {
                total += c.TotalInterestEarned();
            }

            return total;
        }


        public string InterestPaidReport(bool detailed)
        {
            double total = 0, thisInterestPaid = 0;
            string s = "";

            if (customers.Count == 0)
                return "No customers registered";

            if (detailed)
            {
                s = "Interests paid by customer: \n\n";

                foreach (Customer c in customers)
                {
                    thisInterestPaid = c.TotalInterestEarned();
                    total += thisInterestPaid;
                    s += c.GetName() + ": " + ToDollars(thisInterestPaid) + "\n";
                }

                s += "\nTotal interets paid: " + ToDollars(total) + "\n";
            }
            else
            {
                s = "Interests paid consolidated: \n\n";
                foreach (Customer c in customers)
                {
                    total += c.TotalInterestEarned();
                }

                s += "Total interets paid: " + ToDollars(total) + "\n";
            }

            return s;
        }

        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }

        public String GetFirstCustomer()
        {
            if (customers.Count == 0)
                return "No customers registered";
            return customers[0].GetName();
        }
    }
}
