﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    //******************************************************************************************************************************************************
    //It's assumed that a customer cannot have two accounts of the same type
    //******************************************************************************************************************************************************

    public class Account
    {
        //public const int CHECKING = 0;
        //public const int SAVINGS = 1;
        //public const int MAXI_SAVINGS = 2;

        public readonly AccountType accountType;
        public List<Transaction> transactions;

        public Account(AccountType Type)
        {
            this.accountType = Type;
            this.transactions = new List<Transaction>();
        }

        #region "Basic operations"

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction((int)TransactionType.DirectDeposit, amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if(amount > this.balance())
                {
                    throw new ArgumentException("amount exceedes account's balance");
                }
                else
                { 
                    transactions.Add(new Transaction(TransactionType.DirectWithdrawal, amount));
                }
            }
        }

        public void transfer(ref Account dstAccount, double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if(amount > this.balance())
                {
                    throw new ArgumentException("amount exceedes account's balance");
                }
                else
                {
                    //Pulls money
                    this.transactions.Add(new Transaction(TransactionType.OutgoingTransfer, amount));
                    //Pushes money
                    dstAccount.transactions.Add(new Transaction(TransactionType.IncomingTransfer, amount));
                }
            }
        }

        public void transfer(ref Account orgAccount, ref Account dstAccount, double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if (amount > orgAccount.balance())
                {
                    throw new ArgumentException("amount exceedes origin account's balance");
                }
                else
                {
                    //Pulls money
                    orgAccount.transactions.Add(new Transaction(TransactionType.OutgoingTransfer, amount));
                    //Pushes money
                    dstAccount.transactions.Add(new Transaction(TransactionType.IncomingTransfer, amount));
                }
            }
        }

        #endregion


        #region "Additional functions"

        public double InterestEarned()
        {
            //A financial 12mothns-30days = 360 days year is assumed, interest payable rates are stated as annual

            double amount = balance();
            switch (accountType)
            {
                case AccountType.Checking:
                        return amount * 0.001;

                case AccountType.Savings:

                    if (amount <= 1000)
                    {
                        return amount * 0.001;
                    }
                    else
                    {
                        return 1 + ((amount - 1000) * 0.002);
                    }

                case AccountType.MaxiSavings:
                    int ldd = lastDebitDays();

                    if(ldd > 10)
                    {
                        return amount * 0.05;
                    }
                    else
                    {
                        return amount * 0.001;
                    }

                    //if (amount <= 1000)
                    //{
                    //    return amount * 0.02;
                    //}
                    //else if(amount > 1000 && amount <= 2000)
                    //{
                    //    return 20 + ((amount - 1000) * 0.05);
                    //}
                    //else
                    //{
                    //    return 70 + ((amount - 2000)* 0.1);
                    //}

                default:
                    return 0;
            }
        }


        private int lastDebitDays()
        {
            DateTime D = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);

            for(int t = 0; t < transactions.Count; t++)
            {
                if (transactions[t].transactionDate > D && (transactions[t].type == TransactionType.DirectWithdrawal || transactions[t].type == TransactionType.OutgoingTransfer))
                    D = transactions[t].transactionDate;
            }

            return (int)(DateTime.Now - D).TotalDays;
        }


        public double balance()
        {
            double debit = 0, credit = 0;

            for(int t = 0; t < transactions.Count; t++)
            {
                if (transactions[t].type == TransactionType.DirectDeposit || transactions[t].type == TransactionType.IncomingTransfer)
                {
                    credit += transactions[t].amount;
                }
                else
                {
                    debit += transactions[t].amount;
                }
            }

            return credit - debit;
        }

        #endregion

    }
}
