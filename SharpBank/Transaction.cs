﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly TransactionType type;
        public readonly double amount;
        public readonly DateTime transactionDate;

        public Transaction(TransactionType Type, double amount)
        {
            this.type = Type;
            this.amount = amount;
            this.transactionDate = DateTime.Now;
        }
    }
}
